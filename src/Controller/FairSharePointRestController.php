<?php

namespace Foodsharing\Controller;

use Foodsharing\Lib\Session;
use Foodsharing\Modules\FairTeiler\FairSharePointGateway;
use Foodsharing\Services\FairSharePointService;
use Foodsharing\Services\ImageService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Rest controller for fair share points.
 */
final class FairSharePointRestController extends AbstractFOSRestController
{

	private $gateway;
	private $service;
	private $imageService;
	private $session;

	public function __construct(FairSharePointGateway $gateway, FairSharePointService $service, ImageService $imageService, Session $session)
	{
		$this->gateway = $gateway;
		$this->service = $service;
		$this->imageService = $imageService;
		$this->session = $session;
	}

	/**
	 * Returns a list of fair share points depending on the type.
	 * 'mine': lists all fair share points of the current user.
	 * 'coordinates': lists all fair share points IDs together with the coordinates.
	 *
	 * Returns 200 and a list of fair share points or 401 if not logged in.
	 *
	 * @Rest\Get("fairSharePoints")
	 * @Rest\QueryParam(name="type", requirements="(mine|coordinates)", default="mine")
	 *
	 * @param ParamFetcher $paramFetcher
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function listFairSharePointsAction(ParamFetcher $paramFetcher): \Symfony\Component\HttpFoundation\Response
	{
		$this->throwExceptionIfNotLoggedIn();

		$fairSharePoints = [];
		switch ($paramFetcher->get('type')) {
			case 'mine':
				$fairSharePoints = $this->getCurrentUsersFairSharePoints();
				break;
			case 'coordinates':
				$fairSharePoints = $this->gateway->getFairSharePointCoordinates();
				break;
		}

		return $this->handleView($this->view(['fairSharePoints' => $fairSharePoints], 200));
	}

	private function throwExceptionIfNotLoggedIn()
	{
		if (!$this->session->may()) {
			throw new HttpException(401, self::NOT_LOGGED_IN);
		}
	}

	/**
	 * Returns details of the fair share point with the given ID. Returns 200 and the
	 * fair share point, 500 if the fair share point does not exist, or 401 if not logged in.
	 *
	 * @Rest\Get("fairSharePoints/{$fairSharePointId}", requirements={"$fairSharePointId" = "\d+"})
	 *
	 * @param int $fairSharePointId
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function getFairSharePointAction(int $fairSharePointId): \Symfony\Component\HttpFoundation\Response
	{
		$this->throwExceptionIfNotLoggedIn();


		$fairSharePoint = $this->gateway->getFairSharePoint($fairSharePointId);
		$this->throwExceptionIfBasketDoesNotExist($fairSharePointId);

		return $this->handleView($this->view(['fairSharePoint' => $fairSharePoint], 200));
	}

	private function throwExceptionIfBasketDoesNotExist($fairSharePoint)
	{
		if (!$fairSharePoint) {
			throw new HttpException(404, 'FairSharePoint does not exist or was deleted.');
		}
	}

	private function getCurrentUsersFairSharePoints()
	{
		//TODO:
		$fairSharePoint = $this->gateway->getFairSharePoint(75);
		$this->throwExceptionIfBasketDoesNotExist($fairSharePointId);

		return $this->handleView($this->view(['fairSharePoints' => [$fairSharePoint]], 200));
	}
}
