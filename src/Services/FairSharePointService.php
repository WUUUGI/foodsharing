<?php

namespace Foodsharing\Services;

use Foodsharing\Lib\Session;
use Foodsharing\Modules\FairTeiler\FairSharePointGateway;

class FairSharePointService
{
	private $fairSharePointGateway;
	private $session;

	public function __construct(
		FairSharePointGateway $fairSharePointGateway,
		Session $session
	)
	{
		$this->fairSharePointGateway = $fairSharePointGateway;
		$this->session = $session;
	}
}
